package org.hotovo.lac.model.common.mq.queue;

import org.hotovo.lac.model.common.mq.BaseMessage;

/**
 * {@link QueueType#Publisher} queue.
 * 
 * @author Stanislav Dvorscak
 *
 * @param <T>
 *            type of messages
 */
public class Publisher<T extends BaseMessage> extends Queue<T> {

	/**
	 * Constructor.
	 * 
	 * @param address
	 *            {@link #getAddress()}
	 * @param type
	 *            {@link #getType()}
	 */
	public Publisher(String address, Class<T> type) {
		super(QueueType.Publisher, address, type);
	}

}
