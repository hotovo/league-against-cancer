package org.hotovo.lac.view.rest;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * System related REST resource.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Named
@Singleton
@Path("/system")
public class SystemResource {

	/**
	 * Constructor.
	 */
	public SystemResource() {
	}

	/**
	 * @return Test that system is up.
	 */
	@GET
	@Path("/ping")
	public Response ping() {
		return Response.noContent().build();
	}

}
