package org.hotovo.lac.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * XML representation of {@link BaseEntity}.
 * 
 * @author Stanislav Dvorscak
 *
 */
public abstract class BaseEntityDTO implements EntityDTO {

	/**
	 * @see #getId()
	 */
	private Long id;

	/**
	 * Constructor.
	 */
	public BaseEntityDTO() {
	}

	/**
	 * @return {@link BaseEntity#getId()}
	 */
	@XmlElement
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            {@link #getId()}
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
