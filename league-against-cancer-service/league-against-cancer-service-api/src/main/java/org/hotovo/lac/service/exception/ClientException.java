package org.hotovo.lac.service.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ClientException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	public ClientException(String message) {
		super(message);
	}

	public ClientException(Response response) {
		super(response.getStatus() + ":" + response.readEntity(String.class));
	}

	public ClientException(int status) {
		super(status);
	}
}
