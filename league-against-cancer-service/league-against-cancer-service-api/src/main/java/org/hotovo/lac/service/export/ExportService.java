package org.hotovo.lac.service.export;

import java.io.OutputStream;

/**
 * @author Vladimir Hrusovsky
 */
public interface ExportService {
    void exportVariantA(OutputStream outputStream);

    void exportVariantB(OutputStream outputStream);
}
