package org.hotovo.lac.service.collecting;

import org.hotovo.lac.model.collecting.Record;
import org.hotovo.lac.model.collecting.dto.RecordDTO;

/**
 * @author Vladimir Hrusovsky
 */
public interface RecordService {
    Record fromDTO(RecordDTO dto);

    RecordDTO toDTO(Record record);
}
