package org.hotovo.lac.model.collecting.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Vladimir Hrusovsky
 */
@XmlRootElement(name = "record")
public class RecordDTO {
    private Float nominal;
    private Integer count;

    @XmlElement
    public Float getNominal() {
        return nominal;
    }

    public void setNominal(Float nominal) {
        this.nominal = nominal;
    }

    @XmlElement
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
