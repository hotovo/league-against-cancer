#!/bin/sh

###################################################
#  Configuration part
###################################################
SERVICE_NAME=league-against-cancer
NAME="Liga proti rakovine"
#RUN_AS="run_user"
#JAVA_HOME="/etc/alternatives/jre_1.8.0"
ROOT_DIR="$(realpath $(dirname $0)/..)"
VAR_DIR=$ROOT_DIR/var

###################################################
#  Do not edit next parts
###################################################

###################################################
#  Builds minimal directory layout structure
###################################################
mkdir -p $VAR_DIR
mkdir -p $VAR_DIR/run
mkdir -p $VAR_DIR/log

###################################################
#  Permissions correction
###################################################
if [ -n "$RUN_AS" ]; then
	chown $RUN_AS $VAR_DIR
	chown $RUN_AS $VAR_DIR/run
	chown $RUN_AS $VAR_DIR/log
fi

###################################################
#  JSVC Configuration
###################################################
JSVC=$ROOT_DIR/bin/league-against-cancer

PID_FILE=$VAR_DIR/run/$SERVICE_NAME.pid
CLASSPATH="$ROOT_DIR:`ls $ROOT_DIR/lib/*.jar | tr "\\n" ":"`"
MAIN="org.hotovo.lac.assembly.standalone.ApplicationDaemon"

if [ -n "$RUN_AS" ]; then
	CMD_START="$JSVC -server -wait 30 -home $JAVA_HOME -cwd $ROOT_DIR -user $RUN_AS -pidfile $PID_FILE -cp $CLASSPATH $MAIN"
    CMD_STOP="$JSVC -stop -home $JAVA_HOME -cwd $ROOT_DIR -user $RUN_AS -pidfile $PID_FILE -cp $CLASSPATH $MAIN"
    CMD_STATUS="$JSVC -check -home $JAVA_HOME -cwd $ROOT_DIR -user $RUN_AS -pidfile $PID_FILE -cp $CLASSPATH $MAIN"
else
	CMD_START="$JSVC -server -wait 30 -home $JAVA_HOME -cwd $ROOT_DIR -pidfile $PID_FILE -cp $CLASSPATH $MAIN"
    CMD_STOP="$JSVC -stop -home $JAVA_HOME -cwd $ROOT_DIR -pidfile $PID_FILE -cp $CLASSPATH $MAIN"
    CMD_STATUS="$JSVC -check -home $JAVA_HOME -cwd $ROOT_DIR -pidfile $PID_FILE -cp $CLASSPATH $MAIN"
fi

###################################################
#  main()
###################################################
RETVAL=0
case "$1" in
    start)
        printf "%s" "Starting $NAME: "
        if [ ! -s $PID_FILE ]; then
            $CMD_START
            RETVAL=$?
            printf "\n"
        else
            printf "%s\n" "already running"
            RETVAL=1
        fi
    ;;
    stop)
        printf "%s" "Shutting down $NAME: "
        if [ -s $PID_FILE ]; then
            $CMD_STOP
            RETVAL=$?
            printf "\n"
        else
            printf "%s\n" "not running"
        fi
    ;;
    status)
        if [ -s $PID_FILE ]; then
            printf "%s\n" "$NAME is running."
        else
            printf "%s\n" "$NAME is not running."
        fi
    ;;
    restart)
        $0 stop; $0 start
    ;;
    reload)
        $0 stop; $0 start
    ;;
    *)
        printf "%s\n" "Usage: $SERVICE_NAME {start|stop|status|reload|restart}"
        exit 1
    ;;
esac
exit $RETVAL
