package org.hotovo.lac.service;

import org.hotovo.lac.model.common.mq.queue.Publisher;
import org.hotovo.lac.model.common.mq.queue.Subscriber;
import org.hotovo.lac.service.common.mq.RetryMessage;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.time.Duration;
import java.time.Instant;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class RetryMessageProducer {
    public static final String RETRY_MESSAGE_QUEUE = "RetryMessageQueue";

    @Inject
    private volatile MessagingService messagingService;

    private volatile Publisher<RetryMessage> publisher;

    @PostConstruct
    private void onInit() {
        publisher = messagingService.publisher(RETRY_MESSAGE_QUEUE, RetryMessage.class);
    }

    public void registerSubscriber(Subscriber<RetryMessage> consumer) {
        messagingService.connect(publisher, consumer);
    }

    public void retryMessage(String message, String address, Duration afterTime) {
        messagingService.send(publisher, new RetryMessage(address, message, Instant.now().plus(afterTime).toEpochMilli()));
    }

    public void retryMessage(RetryMessage retryMessage) {
        messagingService.send(publisher, retryMessage);
    }
}
