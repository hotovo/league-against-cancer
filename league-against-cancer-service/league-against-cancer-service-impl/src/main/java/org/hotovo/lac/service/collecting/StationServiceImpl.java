package org.hotovo.lac.service.collecting;

import org.hotovo.lac.dao.collecting.StationDAO;
import org.hotovo.lac.model.collecting.Station;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class StationServiceImpl implements StationService {
    @Inject
    private volatile StationDAO stationDAO;

    @Override
    public List<Station> getAll() {
        return stationDAO.getAll();
    }

    @Override
    public Optional<Station> getStationByName(String name) {
        return stationDAO.getStationByName(name);
    }

    @Override
    public void save(Station station) {
        stationDAO.save(station);
    }
}
