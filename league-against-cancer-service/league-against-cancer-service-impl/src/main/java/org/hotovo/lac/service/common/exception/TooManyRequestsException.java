package org.hotovo.lac.service.common.exception;

import javax.ws.rs.ProcessingException;
import java.time.Duration;

/**
 * @author Vladimir Hrusovsky
 */
public class TooManyRequestsException extends ProcessingException {

	/**
	 * Serial version id.
	 */
	private static final long serialVersionUID = 1L;
	
	private final Duration retryWaitTime;

    public TooManyRequestsException(Duration retryWaitTime) {
        super("Too many requests");
        this.retryWaitTime = retryWaitTime;
    }

    public Duration getRetryWaitTime() {
        return retryWaitTime;
    }
}
