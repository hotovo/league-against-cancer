package org.hotovo.lac.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

/**
 * I/O related utilities.
 * 
 * @author Stanislav Dvorscak
 *
 */
public class IOUtils {

	/**
	 * Only static members.
	 */
	private IOUtils() {
	}

	/**
	 * Loads a provided {@link InputStream} content into bytes array.
	 * 
	 * @param input
	 *            for loading
	 * @return loaded content
	 * @throws IOException
	 */
	public static byte[] load(InputStream input) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		ReadableByteChannel inputChannel = Channels.newChannel(input);
		WritableByteChannel outputChannel = Channels.newChannel(output);

		ByteBuffer buffer = ByteBuffer.allocate(64536);
		while (inputChannel.read(buffer) >= 0 || buffer.position() != 0) {
			buffer.flip();
			outputChannel.write(buffer);
			buffer.compact();
		}

		return output.toByteArray();
	}

}
