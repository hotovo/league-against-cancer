package org.hotovo.lac.view.rest.collecting;

import org.hotovo.lac.service.collecting.StationService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Path("stations")
@Transactional
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StationResource {
    @Inject
    private volatile StationService service;

    @GET
    public Response getAll() {
        return Response.ok(service.getAll()).build();
    }
}
