package org.hotovo.lac.service.collecting;

import org.hotovo.lac.dao.collecting.CollectionBoxDAO;
import org.hotovo.lac.model.collecting.CollectionBox;
import org.hotovo.lac.model.collecting.Station;
import org.hotovo.lac.model.collecting.dto.CollectionBoxDTO;
import org.hotovo.lac.model.user.User;
import org.hotovo.lac.service.user.UserService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class CollectionBoxServiceImpl implements CollectionBoxService {
    @Inject
    private volatile CollectionBoxDAO boxDAO;
    @Inject
    private volatile UserService userService;
    @Inject
    private volatile StationService stationService;
    @Inject
    private volatile RecordService recordService;

    @Override
    public List<CollectionBox> getAll() {
        return boxDAO.getAll();
    }

    @Override
    public Optional<CollectionBox> getBox(Long boxID) {
        return boxDAO.getBoxById(boxID);
    }

    @Override
    public void save(CollectionBox box) {
        boxDAO.save(box);
    }

    @Override
    public CollectionBox fromDTO(CollectionBoxDTO box) {
        final CollectionBox collectionBox = new CollectionBox();

        collectionBox.setId(box.getId());
        collectionBox.setNumber(box.getNumber());
        collectionBox.setRecorder(getRecorderUser(box.getRecorderUserName()));
        collectionBox.setStation(getStation(box.getStationName()));
        collectionBox.setRecords(
                box.getRecords().stream()
                        .map(recordService::fromDTO)
                        .collect(Collectors.toList())
        );

        return collectionBox;
    }

    @Override
    public CollectionBoxDTO toDTO(CollectionBox box) {
        final CollectionBoxDTO dto = new CollectionBoxDTO();

        dto.setId(box.getId());
        dto.setNumber(box.getNumber());
        dto.setRecorderUserName(box.getRecorder() != null ? box.getRecorder().getUserName() : null);
        dto.setStationName(box.getStation() != null ? box.getStation().getName() : null);
        dto.setRecords(
                box.getRecords().stream()
                        .map(recordService::toDTO)
                        .collect(Collectors.toList())
        );

        return dto;
    }


    private User getRecorderUser(String recorderUserName) {
        final Optional<User> user = userService.getUserByUserName(recorderUserName);

        if (user.isPresent()) {
            return user.get();
        }

        final User newUser = new User();
        newUser.setUserName(recorderUserName);
        userService.save(newUser);

        return newUser;
    }

    private Station getStation(String stationName) {
        final Optional<Station> station = stationService.getStationByName(stationName);

        if (station.isPresent()) {
            return station.get();
        }

        final Station newStation = new Station();
        newStation.setName(stationName);
        stationService.save(newStation);

        return newStation;
    }
}
