package org.hotovo.lac.dao.user;

import org.hotovo.lac.model.user.User;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface UserDAO {
    List<User> getAll();

    void save(User user);

    void delete(User user);

    User getUserById(long id);

    Optional<User> getUserByUserName(String userName);
}
