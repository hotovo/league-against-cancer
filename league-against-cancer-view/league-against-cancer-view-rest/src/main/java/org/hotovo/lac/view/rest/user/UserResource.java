package org.hotovo.lac.view.rest.user;

import org.hotovo.lac.model.user.User;
import org.hotovo.lac.model.user.dto.UserDTO;
import org.hotovo.lac.service.user.UserService;
import org.hotovo.lac.view.rest.aop.Authorization;
import org.hotovo.lac.view.rest.aop.Role;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Path("users")
@Transactional
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    @Inject
    private volatile UserService userService;

    @GET
    @Authorization(Role.ADMIN)
    public Response getAll() {
        return Response.ok(userService.getAll()).build();
    }

    @GET
    @Path("/{userId}")
    @Authorization(Role.ADMIN)
    public Response getUser(@PathParam("userId") Long userId) {
        final User user = userService.getUserById(userId);
        if (user == null) {
            return userNotFoundResponse(userId);
        }

        return Response.ok(userService.toDTO(user)).build();
    }

    @POST
    @Authorization(Role.ADMIN)
    public Response save(UserDTO userDTO) {
        final User user = userService.fromDTO(userDTO);
        final Optional<User> existingUser = userService.getUserByUserName(user.getUserName());

        if (existingUser.isPresent()) {
            return usernameExistsResponse(user.getUserName());
        }
        final String password = generatePassword();
        user.setPassword(userService.hashPassword(password));
        userService.save(user);

        return Response.ok(userService.toDTO(user.getId(), password, true)).build();
    }

    private String generatePassword() {
        final SecureRandom random = new SecureRandom();
        return new BigInteger(64, random).toString(32);
    }

    @PUT
    @Path("/{userId}")
    @Authorization(Role.ADMIN)
    public Response save(UserDTO userDTO, @PathParam("userId") Long userId) {
        final User user = userService.getUserById(userId);
        if (user == null) {
            return userNotFoundResponse(userId);
        }
        userService.save(user);
        return Response.ok(userService.toDTO(user)).build();
    }

    @PUT
    @Path("/{userId}/password")
    @Authorization(Role.USER)
    public Response setPassword(UserDTO userDTO, @PathParam("userId") Long userId) {
        final User user = userService.getUserById(userId);
        if (user == null) {
            return userNotFoundResponse(userId);
        }

        if (userDTO.getPassword() != null) {
            user.setPassword(userService.hashPassword(userDTO.getPassword()));
        }
        userService.save(user);
        return Response.ok(userService.toDTO(user)).build();
    }

    @GET
    @Path("/{userId}/reset")
    @Authorization(Role.ADMIN)
    public Response resetPassword(@PathParam("userId") Long userId) {
        final User user = userService.getUserById(userId);
        if (user == null) {
            return userNotFoundResponse(userId);
        }

        final String password = generatePassword();
        user.setPassword(userService.hashPassword(password));
        userService.save(user);

        return Response.ok(userService.toDTO(user.getId(), password, true)).build();
    }

    @DELETE
    @Path("/{userId}")
    @Authorization(Role.ADMIN)
    public Response delete(@PathParam("userId") Long userId) {
        final User user = userService.getUserById(userId);
        if (user == null) {
            return userNotFoundResponse(userId);
        }
        userService.delete(user);
        return Response.ok().build();
    }

    @GET
    @Path("/user_authorized")
    @Authorization(Role.PUBLIC)
    public Response isUserAuthorized() {
        if (userService.isUserAuthorized()) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/admin_authorized")
    @Authorization(Role.PUBLIC)
    public Response isAdminAuthorized() {
        if (userService.isAdminAuthorized()) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @PUT
    @Path("/login")
    @Authorization(Role.PUBLIC)
    public Response login(UserDTO userDTO) {
        final User authenticateUser = userService.login(userDTO.getUserName(), userDTO.getPassword());
        if (authenticateUser != null) {
            return Response.ok(userService.toDTO(authenticateUser)).build();
        } else {
            return wrongAuthentication();
        }
    }



    @GET
    @Path("/logout")
    public Response logout() {
        userService.logout();
        return Response.ok().build();
    }

    private Response wrongAuthentication() {
        return Response.status(Response.Status.UNAUTHORIZED)
                .entity(Collections.singletonMap("message", "Wrong username or password"))
                .build();
    }

    private Response userNotFoundResponse(long id) {
        return Response.status(Response.Status.NOT_FOUND)
                .entity(Collections.singletonMap("message", "There is no User for a provided id: " + id))
                .build();
    }

    private Response usernameExistsResponse(String username) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(Collections.singletonMap("message", "User with username already exists: " + username))
                .build();
    }
}
