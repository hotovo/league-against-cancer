package org.hotovo.lac.service.collecting;

import org.hotovo.lac.model.collecting.Record;
import org.hotovo.lac.model.collecting.dto.RecordDTO;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class RecordServiceImpl implements RecordService {
    @Override
    public Record fromDTO(RecordDTO recordDTO) {
        final Record record = new Record();
        record.setNominal(recordDTO.getNominal());
        record.setCount(recordDTO.getCount());
        return record;
    }

    @Override
    public RecordDTO toDTO(Record record) {
        final RecordDTO dto = new RecordDTO();
        dto.setNominal(record.getNominal());
        dto.setCount(record.getCount());
        return dto;
    }
}
