package org.hotovo.lac.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base class for all other entities.
 * 
 * @author Stanislav Dvorscak
 *
 */
@MappedSuperclass
public abstract class BaseEntity implements Entity {

	/**
	 * @see #getId()
	 */
	private Long id;

	/**
	 * Constructor.
	 */
	public BaseEntity() {
	}

	/**
	 * @return Identity of entity.
	 */
	@Id
	@GeneratedValue
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            {@link #getId()}
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
