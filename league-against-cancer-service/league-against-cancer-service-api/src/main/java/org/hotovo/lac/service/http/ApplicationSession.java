package org.hotovo.lac.service.http;

import org.hotovo.lac.model.user.User;
import org.hotovo.lac.service.javax.inject.Session;

import javax.inject.Named;

/**
 * Holds session related information.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Named
@Session
public class ApplicationSession {

	/**
	 * @see #getUsername()
	 */
	private volatile String username;

	/**
	 * Constructor.
	 */
	public ApplicationSession() {
	}

	/**
	 * @return {@link User#getUserName()} of current logged in user.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            {@link #getUsername()}
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Cleares session information.
	 */
	public void clear() {
		username = null;
	}

}
