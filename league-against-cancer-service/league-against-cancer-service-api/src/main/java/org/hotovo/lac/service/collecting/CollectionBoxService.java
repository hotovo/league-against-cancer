package org.hotovo.lac.service.collecting;

import org.hotovo.lac.model.collecting.CollectionBox;
import org.hotovo.lac.model.collecting.dto.CollectionBoxDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface CollectionBoxService {
    List<CollectionBox> getAll();

    Optional<CollectionBox> getBox(Long boxID);

    void save(CollectionBox box);

    CollectionBox fromDTO(CollectionBoxDTO box);

    CollectionBoxDTO toDTO(CollectionBox box);
}
