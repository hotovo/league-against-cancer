package org.hotovo.lac.assembly.standalone.module;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Jsr330ScopeMetadataResolver;

/**
 *
 * @author Vladimir Hrusovsky
 *
 */
@Configuration
@ComponentScan(basePackages = "org.hotovo.lac.websocket", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class WebSocketModule {
}
