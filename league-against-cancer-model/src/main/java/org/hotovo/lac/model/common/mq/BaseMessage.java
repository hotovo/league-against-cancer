package org.hotovo.lac.model.common.mq;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Base class for all other messages.
 * 
 * @author Stanislav Dvorscak
 *
 */
@XmlRootElement
public abstract class BaseMessage {
    private int retryCount = 0;

    @XmlElement
    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }
}
