package org.hotovo.lac.assembly.standalone.module;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Jsr330ScopeMetadataResolver;

/**
 * Starts application minimum - useful for migration purposes, and other staff.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Configuration
@ComponentScan(basePackages = "org.hotovo.lac.system", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class SystemModule {
}
