package org.hotovo.lac.view.rest.collecting;

import org.hotovo.lac.model.collecting.CollectionBox;
import org.hotovo.lac.model.collecting.dto.CollectionBoxDTO;
import org.hotovo.lac.service.collecting.CollectionBoxService;
import org.hotovo.lac.service.export.ExportService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Path("collection_boxes")
@Transactional
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CollectionBoxResource {
    @Inject
    private volatile CollectionBoxService service;
    @Inject
    private volatile ExportService exportService;

    @GET
    public Response getAll() {
        return Response.ok(
                service.getAll().stream()
                .map(service::toDTO)
                .collect(Collectors.toList())
        ).build();
    }

    @GET
    @Path("/{boxID}")
    public Response getCollectionBox(@PathParam("boxID") Long boxID) {
        final Optional<CollectionBox> box = service.getBox(boxID);
        return box.isPresent() ? Response.ok(service.toDTO(box.get())).build() : notFoundResponse(boxID);

    }

    @POST
    public Response save(CollectionBoxDTO box) {
        service.save(service.fromDTO(box));
        return Response.ok(box).build();
    }

    @GET
    @Produces("application/xls")
    @Path("/exportA.xls")
    public StreamingOutput exportA() {
        return output -> exportService.exportVariantA(output);
    }

    @GET
    @Produces("application/xls")
    @Path("/exportB.xls")
    public StreamingOutput exportB() {
        return output -> exportService.exportVariantB(output);
    }

    private Response notFoundResponse(long id) {
        return Response.status(Response.Status.NOT_FOUND)
                .entity(Collections.singletonMap("message", "There is no User for a provided id: " + id))
                .build();
    }
}
