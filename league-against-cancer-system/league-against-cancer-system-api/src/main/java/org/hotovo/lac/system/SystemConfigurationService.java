package org.hotovo.lac.system;

import javax.sql.DataSource;

/**
 * Provides access to system configuration.
 *
 * @author Stanislav Dvorscak
 *
 */
public interface SystemConfigurationService {

	/**
	 * Properties file.
	 */
	String PROPERTIES_FILE = "etc/league-against-cancer.properties";

    /**
     * Properties key for {@link #getServerHost()}.
     */
    String SERVER_HOST = "server.host";

	/**
	 * Properties key for {@link #getVarDir()}.
	 */
	String VAR_DIR = "var.dir";

	/**
	 * Properties key for a {@link #getJDBCDriver()}.
	 */
	String JDBC_DRIVER = "jdbc.driver";

	/**
	 * Properties key for a {@link #getJDBCUrl()}.
	 */
	String JDBC_URL = "jdbc.url";

	/**
	 * Properties key for a {@link #getJDBCUsername()}.
	 */
	String JDBC_USERNAME = "jdbc.username";

	/**
	 * Properties key for a {@link #getJDBCPassword()}.
	 */
	String JDBC_PASSWORD = "jdbc.password";

	/**
	 * Properties key for a #get
	 */
	String HIBERNATE_DIALECT = "hibernate.dialect";

	/**
	 * Properties key for a {@link #getServiceHttpPort()}.
	 */
	String SERVICE_HTTP_PORT = "service.http.port";

	/**
	 * Properties key for a {@link #getServiceHttpContextPath()}.
	 */
	String SERVICE_HTTP_CONTEXT_PATH = "service.http.contextPath";

	/**
	 * Properties key for a {@link #getUrlConnectionTimeout()}.
	 */
	String URL_CONNECTION_TIMEOUT = "url.connection.timeout";

	/**
	 * Properties key for a {@link #getUrlConnectionReadTimeout()}.
	 */
	String URL_CONNECTION_READ_TIMEOUT = "url.connection.read.timeout";

    /**
     * @return server host address accessible from public
     */
    String getServerHost();

	/**
	 * @return Returns relative directory for a variable data.
	 */
	String getVarDir();

	/**
	 * @return JDBC driver class used by {@link DataSource}.
	 */
	String getJDBCDriver();

	/**
	 * @return JDBC URL.
	 */
	String getJDBCUrl();

	/**
	 * @return Username needed for DB authentication / authorization.
	 */
	String getJDBCUsername();

	/**
	 * @return Password needed for DB authentication / authorization.
	 */
	String getJDBCPassword();

	/**
	 * @return Hibernate dialect - related to the {@link #getJDBCDriver()} configuration.
	 */
	String getHibernateDialect();

	/**
	 * @return returns port of the HTTP service.
	 */
	int getServiceHttpPort();

	/**
	 * @return returns context path for the HTTP service.
	 */
	String getServiceHttpContextPath();

	/**
	 * @return url connection timeout in millis
	 */
	int getUrlConnectionTimeout();

	/**
	 * @return url connection read timeout in millis
	 */
	int getUrlConnectionReadTimeout();
}
