package org.hotovo.lac.service;

import org.hotovo.lac.model.common.mq.BaseMessage;
import org.hotovo.lac.model.common.mq.queue.Publisher;
import org.hotovo.lac.model.common.mq.queue.Subscriber;
import org.hotovo.lac.model.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import javax.transaction.Transactional;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

/**
 * A {@link MessagingService} implementation.
 *
 * @author Stanislav Dvorscak
 *
 */
@Named
@Singleton
@Transactional
public class MessagingServiceImpl implements MessagingService {
    private static final Duration RETRY_DELAY_TIME = Duration.ofMinutes(15);
    private static final int MAX_RETRY_COUNT = 10;

    /**
     * Logger for this class.
     */
    private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * {@link Publisher} by its {@link Publisher#getAddress()}.
	 */
	private final Map<String, Publisher<?>> publisherByAddress = new ConcurrentHashMap<>();

	/**
	 * {@link Subscriber} by its {@link Subscriber#getAddress()}.
	 */
	private final Map<String, Subscriber<?>> subscribersByAddress = new ConcurrentHashMap<>();

	/**
	 * {@link Subscriber}-s by connected {@link Publisher}-s / {@link #connect(Publisher, Subscriber)}.
	 */
	private final Map<Publisher<?>, List<Subscriber<?>>> subscribersByPublisher = new ConcurrentHashMap<>();

	/**
	 * Injected {@link MarshallerService} dependency.
	 */
	@Inject
	private volatile MarshallerService marshallerService;

	/**
	 * Injected {@link JmsTemplate} dependency.
	 */
	@Inject
	private volatile JmsTemplate jmsTemplate;

	/**
	 * Injected unlimited {@link ExecutorService} dependency.
	 */
	@Inject
	@Named("unlimited")
	private volatile ExecutorService unlimitedExecutorService;

    /**
     * Injected producer for message retrying.
     */
    @Inject
    private volatile RetryMessageProducer retryMessageProducer;

	/**
	 * Is this bean in a destroying phase?
	 */
	private volatile boolean destroying;

	/**
	 * Counts active subscribers.
	 */
	private volatile Semaphore integerMaxMinusActiveSubscribers = new Semaphore(Integer.MAX_VALUE);

	/**
	 * Constructor.
	 */
	public MessagingServiceImpl() {
	}

	/**
	 * Destroys this bean.
	 */
	@PreDestroy
	public synchronized void onDestroy() {
		destroying = true;
		try {
			integerMaxMinusActiveSubscribers.acquire(Integer.MAX_VALUE);
		} catch (InterruptedException e) {
			throw new ApplicationException("Active subscribers was not shutdown properly:", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseMessage> Publisher<T> publisher(String address, Class<T> type) {
		Publisher<T> result = new Publisher<T>(address, type);
		if (publisherByAddress.put(address, result) != null) {
			throw new IllegalArgumentException("There is already registered a publisher for the provided address: " + address);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseMessage> Subscriber<T> subscriber(String address, Class<T> type) {
		Subscriber<T> result = new Subscriber<T>(address, type);
		if (subscribersByAddress.put(address, result) != null) {
			throw new IllegalArgumentException("There is already registered a subscriber for the provided address: " + address);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseMessage> void connect(Publisher<T> publisher, Subscriber<T> subscriber) {
		subscribersByPublisher.computeIfAbsent(publisher, p -> new CopyOnWriteArrayList<>()).add(subscriber);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseMessage> void send(Publisher<T> publisher, T message) {
		subscribersByPublisher.getOrDefault(publisher, Collections.emptyList()).forEach(subscriber -> {
            jmsTemplate.send(subscriber.getAddress(), session -> {
                String textMessage = marshallerService.toJSON(message);
                return session.createTextMessage(textMessage);
            });
        });
	}

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(String address, String message) {
        jmsTemplate.send(address, session -> {
            return session.createTextMessage(message);
        });
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(Transactional.TxType.NEVER)
	public synchronized <T extends BaseMessage> void receive(Subscriber<T> sub, Consumer<T> onMessage) {
		unlimitedExecutorService.execute(() -> {
			try {
				integerMaxMinusActiveSubscribers.acquire();
			} catch (InterruptedException e) {
				throw new ApplicationException("There is more than Integer#MAX_VALUE active subscribers?", e);
			}
			try {
				while (!destroying) {
					TextMessage textMessage = (TextMessage) jmsTemplate.receive(sub.getAddress());
					if (textMessage != null) {
						String jsonMessage;
						try {
							jsonMessage = textMessage.getText();
						} catch (JMSException e) {
							throw new ApplicationException("Can not deserialize message: ", e);
						}
						T message = marshallerService.fromJSON(sub.getType(), jsonMessage);
                        try {
                            onMessage.accept(message);
                        } catch (Throwable throwable) {
                            if (message.getRetryCount() < MAX_RETRY_COUNT) {
                                logger.error("Exception occurred on message processing...will retry the message again after 15 minutes", throwable);
                                message.setRetryCount(message.getRetryCount() + 1);
                                retryMessageProducer.retryMessage(marshallerService.toJSON(message), sub.getAddress(), RETRY_DELAY_TIME);
                            } else {
                                logger.error("Exception occurred on message processing...max try count exceeded, won't retry anymore", throwable);
                            }
                        }
                        try {
							textMessage.acknowledge();
						} catch (JMSException e) {
							throw new ApplicationException("Can not acknowledge message: ", e);
						}
					}
				}
			} finally {
				integerMaxMinusActiveSubscribers.release();
			}
		});
	}
}
