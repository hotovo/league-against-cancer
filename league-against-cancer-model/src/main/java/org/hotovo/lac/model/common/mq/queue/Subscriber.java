package org.hotovo.lac.model.common.mq.queue;

import org.hotovo.lac.model.common.mq.BaseMessage;

/**
 * {@link QueueType#Subscriber} queue type.
 * 
 * @author Stanislav Dvorscak
 *
 * @param <T>
 *            type of messages
 */
public class Subscriber<T extends BaseMessage> extends Queue<T> {

	/**
	 * Constructor.
	 * 
	 * @param address
	 *            {@link #getAddress()}
	 * @param type
	 *            {@link #getType()}
	 */
	public Subscriber(String address, Class<T> type) {
		super(QueueType.Subscriber, address, type);
	}

}
