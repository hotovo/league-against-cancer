package org.hotovo.lac.service;

import org.hotovo.lac.model.common.mq.BaseMessage;
import org.hotovo.lac.model.common.mq.queue.Publisher;
import org.hotovo.lac.model.common.mq.queue.Subscriber;

import java.util.function.Consumer;

/**
 * Messaging service.
 * 
 * @author Stanislav Dvorscak
 *
 */
public interface MessagingService {

	/**
	 * Queue for message publishing.
	 * 
	 * @param address
	 *            unique address for publisher
	 * @param type
	 *            of published message
	 * @return {@link Publisher}
	 */
	<T extends BaseMessage> Publisher<T> publisher(String address, Class<T> type);

	/**
	 * Queue for message subscribing.
	 * 
	 * @param address
	 *            unique address for subscriber
	 * @param type
	 *            of subscribed message
	 * @return
	 */
	<T extends BaseMessage> Subscriber<T> subscriber(String address, Class<T> type);

	/**
	 * Starts messages routing between a provided publisher and subscriber.
	 * 
	 * @param publisher
	 *            messages source
	 * @param subscriber
	 *            messages target
	 */
	<T extends BaseMessage> void connect(Publisher<T> publisher, Subscriber<T> subscriber);

	/**
	 * Sends a provided message via a provided publisher.
	 * 
	 * @param publisher
	 *            queue
	 * @param message
	 *            for sending
	 */
	<T extends BaseMessage> void send(Publisher<T> publisher, T message);

    /**
     * Sends a provided message via a provided address.
     *
     * @param address
     *            queue address
     * @param message
     *            for sending
     */
    void send(String address, String message);

    /**
	 * Starts asynchronous messages receiving from a provided subscriber.
	 * 
	 * @param subscriber
	 *            queue
	 * @param onMessage
	 *            listener
	 */
	<T extends BaseMessage> void receive(Subscriber<T> subscriber, Consumer<T> onMessage);

}
