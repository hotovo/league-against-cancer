package org.hotovo.lac.model;

/**
 * @author Vladimir Hrusovsky
 */
public interface EntityDTO {
	Long getId();
}
