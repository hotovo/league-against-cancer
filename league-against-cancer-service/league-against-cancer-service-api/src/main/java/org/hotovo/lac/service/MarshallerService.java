package org.hotovo.lac.service;

/**
 * Provides marshalling support.
 * 
 * @author Stanislav Dvorscak
 *
 */
public interface MarshallerService {

	/**
	 * Converts a provided Object to JSON form.
	 * 
	 * @param object
	 *            for transformation
	 * @return JSON message
	 */
	String toJSON(Object object);

	/**
	 * Converts a provided JSON form to an Object of a provided type.
	 * 
	 * @param type
	 *            of object
	 * @param json
	 *            representation
	 * @return converted object
	 */
	<T> T fromJSON(Class<T> type, String json);

}
