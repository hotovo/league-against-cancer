package org.hotovo.lac.service;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;

import org.hotovo.lac.model.exception.ApplicationException;

/**
 * A {@link MarshallerService} implementation.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Named
@Singleton
public class MarshallerServiceImpl implements MarshallerService {

	/**
	 * Cache for {@link JAXBContext}-s resolved by appropriate type.
	 */
	private final Map<Class<?>, JAXBContext> contextByClass = new ConcurrentHashMap<>();

	/**
	 * Constructor.
	 */
	public MarshallerServiceImpl() {
	}

	/**
	 * @param type
	 *            of class for which will be build up a {@link JAXBContext}.
	 * @return
	 */
	public JAXBContext context(Class<?> type) {
		return contextByClass.computeIfAbsent(type, (key) -> {
			try {
				return JAXBContextFactory.createContext(new Class<?>[] { type }, new HashMap<>());
			} catch (Exception e) {
				throw new ApplicationException("Can not build up JAXB Context for a provided type: " + type, e);
			}
		});
	}

	/**
	 * @param type
	 *            of object which will be marshalled
	 * @return marshaller
	 */
	private Marshaller jsonMarshaller(Class<?> type) {
		try {
			Marshaller result = context(type).createMarshaller();
			result.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			result.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
			result.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, false);
			return result;
		} catch (JAXBException e) {
			throw new ApplicationException("Can not build up JSON marshaller for a provided type: " + type, e);
		}
	}

	/**
	 * @param type
	 *            of object which will be unmarshalled
	 * @return unmarshaller
	 */
	private Unmarshaller jsonUnmarshaller(Class<?> type) {
		try {
			Unmarshaller result = context(type).createUnmarshaller();
			result.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
			result.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, false);
			return result;
		} catch (JAXBException e) {
			throw new ApplicationException("Can not build up JSON unmarshaller for a provided type: " + type, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toJSON(Object object) {
		StringWriter result = new StringWriter();
		try {
			jsonMarshaller(object.getClass()).marshal(object, result);
		} catch (JAXBException e) {
			throw new ApplicationException("Can not serialize provided object:", e);
		}
		return result.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T fromJSON(Class<T> type, String json) {
		StreamSource source = new StreamSource(new StringReader(json));
		try {
			return jsonUnmarshaller(type).unmarshal(source, type).getValue();
		} catch (JAXBException e) {
			throw new ApplicationException("Can not deserialize provided JSON:", e);
		}
	}

}
