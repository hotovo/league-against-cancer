package org.hotovo.lac.service;

import org.hotovo.lac.model.common.mq.queue.Subscriber;
import org.hotovo.lac.service.common.mq.RetryMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.time.Duration;
import java.time.Instant;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class RetryMessageConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(RetryMessageConsumer.class);

    @Inject
    private volatile MessagingService messagingService;
    @Inject
    private volatile RetryMessageProducer retryMessageProducer;

    @PostConstruct
    private void onInit() {
        final Subscriber<RetryMessage> subscriber = messagingService.subscriber(RetryMessageProducer.RETRY_MESSAGE_QUEUE, RetryMessage.class);
        retryMessageProducer.registerSubscriber(subscriber);
        messagingService.receive(subscriber, this::processRetryMessage);
    }

    /**
     * Consumer main method
     *
     * @param retryMessage message to process
     */
    private void processRetryMessage(RetryMessage retryMessage) {
        if (!canProcessMessage(retryMessage)) {
            return;
        }
        LOGGER.info("Retrying message on queue " + retryMessage.getAddress() + ":\n" + retryMessage.getMessage());
        messagingService.send(retryMessage.getAddress(), retryMessage.getMessage());
    }

    private boolean canProcessMessage(RetryMessage retryMessage) {
        if (retryMessage.getNextRetryTime() > Instant.now().toEpochMilli()) {
            //add it again to the queue and try later
            retryMessageProducer.retryMessage(retryMessage);
            try {
                Thread.sleep(Duration.ofSeconds(10).toMillis());
            } catch (InterruptedException e) {
                LOGGER.warn("Thread sleep interrupted");
            }
            return false;
        } else {
            return true;
        }
    }
}
