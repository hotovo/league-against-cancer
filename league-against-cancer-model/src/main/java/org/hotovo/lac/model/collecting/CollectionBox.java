package org.hotovo.lac.model.collecting;

import org.hotovo.lac.model.BaseEntity;
import org.hotovo.lac.model.user.User;

import javax.persistence.*;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
@Entity
@Table(name = "collection_boxes")
public class CollectionBox extends BaseEntity {
    private String number;
    private User recorder;
    private Station station;
    private List<Record> records;

    @Column(name = "number")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @ManyToOne
    @JoinColumn(name = "recorder_id", nullable = false)
    public User getRecorder() {
        return recorder;
    }

    public void setRecorder(User recorder) {
        this.recorder = recorder;
    }

    @ManyToOne
    @JoinColumn(name = "station_id", nullable = true)
    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "box_id")
    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
