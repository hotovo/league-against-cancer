package org.hotovo.lac.assembly.standalone.module;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Jsr330ScopeMetadataResolver;

/**
 * Setups DAO layer.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Configuration
@ComponentScan(basePackages = "org.hotovo.lac.dao", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class DAOModule {
}
