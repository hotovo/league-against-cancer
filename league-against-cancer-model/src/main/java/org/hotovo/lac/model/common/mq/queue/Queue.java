package org.hotovo.lac.model.common.mq.queue;

import java.util.Objects;

import org.hotovo.lac.model.common.mq.BaseMessage;

/**
 * Base class for queues.
 * 
 * @author Stanislav Dvorscak
 *
 * @param <T>
 *            type of messages
 */
public abstract class Queue<T extends BaseMessage> {

	/**
	 * Type of queue.
	 * 
	 * @see Queue#getQueueType()
	 * @author Stanislav Dvorscak
	 *
	 */
	public enum QueueType {
		Publisher, Subscriber
	}

	/**
	 * @see #getQueueType()
	 */
	private final QueueType queueType;

	/**
	 * @see #getAddress()
	 */
	private final String address;

	/**
	 * @see #getType()
	 */
	private final Class<T> type;

	/**
	 * Constructor.
	 * 
	 * @param queueType
	 *            {@link #getQueueType()}
	 * @param address
	 *            {@link #getAddress()}
	 * @param type
	 *            {@link #getType()}
	 * @throws IllegalArgumentException
	 *             if queue type or address is not provided
	 */
	public Queue(QueueType queueType, String address, Class<T> type) {
		if (queueType == null) {
			throw new IllegalArgumentException("Queue type can not be null!");
		}
		if (address == null || address.isEmpty()) {
			throw new IllegalArgumentException("Address of queue can not be null or blank!");
		}
		if (type == null) {
			throw new IllegalArgumentException("Messages type can not be null!");
		}

		this.queueType = queueType;
		this.address = address;
		this.type = type;
	}

	/**
	 * @return type of queue
	 */
	public QueueType getQueueType() {
		return queueType;
	}

	/**
	 * @return Unique queue identifier.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return type of messages
	 */
	public Class<T> getType() {
		return type;
	}

	/**
	 * {@link #getQueueType() queue type} and {@link #getAddress() address} based equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Queue<?>) {
			Queue<?> queue = (Queue<?>) obj;
			return Objects.equals(queueType, queue.getQueueType()) && Objects.equals(address, queue.getAddress());
		} else {
			return super.equals(obj);
		}
	}

	/**
	 * {@link #getQueueType() queue type} and {@link #getAddress() address} based hash code.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(queueType, address);
	}

}
