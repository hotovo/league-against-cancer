package org.hotovo.lac.assembly.standalone.module;

import org.hotovo.lac.assembly.standalone.service.HttpService.FilterProvider;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jetty.servlet.FilterHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.RequestContextFilter;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.DispatcherType;
import javax.ws.rs.Path;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import java.util.EnumSet;

/**
 * Loads REST/JAX-RS module.
 *
 * @author Stanislav Dvorscak
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "org.hotovo.lac.view.rest", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class JAXRSModule {

    /**
     * Relative URI to REST filter.
     */
    private static final String REST_FILTER_URI = "/rest";

    /**
     * Constructor.
     */
    public JAXRSModule() {
    }

    /**
     * @return {@link FilterProvider} for a JAX-RS.
     */
    @Bean
    public FilterProvider jaxRSFilter() {
        return new FilterProvider() {

            @Override
            public FilterHolder getHolder() {
                FilterHolder filterHolder = new FilterHolder(ServletContainer.class);
                filterHolder.setInitParameter("javax.ws.rs.Application", JAXRSApplication.class.getName());
                filterHolder.setInitParameter("jersey.config.servlet.filter.contextPath", REST_FILTER_URI);
                return filterHolder;
            }

            @Override
            public String getPathSpecs() {
                return REST_FILTER_URI + "/*";
            }

            @Override
            public EnumSet<DispatcherType> getDispatches() {
                return EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST);
            }
        };
    }

    /**
     * @return {@link FilterProvider} for Request context filter
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public FilterProvider requestFilter() {
        return new FilterProvider() {
            @Override
            public FilterHolder getHolder() {
                return new FilterHolder(RequestContextFilter.class);
            }

            @Override
            public String getPathSpecs() {
                return REST_FILTER_URI + "/*";
            }

            @Override
            public EnumSet<DispatcherType> getDispatches() {
                return EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST);
            }
        };
    }

    /**
     * JAX-RS application.
     *
     * @author Stanislav Dvorscak
     */
    @Named
    @Singleton
    public static class JAXRSApplication extends ResourceConfig {

        /**
         * Injected {@link ApplicationContext} dependency.
         */
        @Inject
        private volatile ApplicationContext applicationContext;

        /**
         * Constructor.
         */
        public JAXRSApplication() {
        }

        /**
         * Registers JAX-RS providers and resources.
         */
        @PostConstruct
        public void init() {
            applicationContext.getBeansWithAnnotation(Provider.class).forEach((name, provider) -> register(provider));
            applicationContext.getBeansWithAnnotation(Path.class).forEach((name, resource) -> register(resource));

            register(JaxbObjectMapperProvider.class);
        }
    }

    public static class JaxbObjectMapperProvider implements ContextResolver<ObjectMapper> {

        private final ObjectMapper mapper;

        /**
         * Constructor.
         */
        public JaxbObjectMapperProvider() {
            this.mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }

        @Override
        public ObjectMapper getContext(Class<?> type) {
            return mapper;
        }

    }
}
