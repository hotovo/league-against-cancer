package org.hotovo.lac.dao.collecting;

import org.hotovo.lac.model.collecting.CollectionBox;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface CollectionBoxDAO {
    List<CollectionBox> getAll();

    void save(CollectionBox box);

    Optional<CollectionBox> getBoxById(long id);
}
