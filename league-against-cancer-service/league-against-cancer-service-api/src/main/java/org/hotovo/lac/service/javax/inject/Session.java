package org.hotovo.lac.service.javax.inject;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

/**
 * Session scope, which is a <i>javax.inject</i> scope equivalent for a <i>spring session scope</i>.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Scope
@Documented
@Retention(RUNTIME)
public @interface Session {
}
