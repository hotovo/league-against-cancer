package org.hotovo.lac.service.common.filter;

import org.hotovo.lac.service.common.exception.TooManyRequestsException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import java.io.IOException;
import java.time.Duration;

/**
 * @author Vladimir Hrusovsky
 */
public class TooManyRequestsFilter implements ClientResponseFilter {
    private static final int STATUS_TOO_MANY_REQUESTS = 429;

    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
        if (responseContext.getStatus() != STATUS_TOO_MANY_REQUESTS) {
            return;
        }

        throw new TooManyRequestsException(getRetryWaitTime(responseContext));
    }

    private Duration getRetryWaitTime(ClientResponseContext responseContext) {
        final String retryAfter = responseContext.getHeaders().getFirst(HttpHeaders.RETRY_AFTER);
        return Duration.ofSeconds(Long.parseLong(retryAfter));
    }
}
