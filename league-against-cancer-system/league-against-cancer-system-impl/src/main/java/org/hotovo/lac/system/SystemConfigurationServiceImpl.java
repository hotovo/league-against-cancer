package org.hotovo.lac.system;

import org.hotovo.lac.model.exception.FatalApplicationException;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * A {@link SystemConfigurationService} implementation.
 *
 * @author Stanislav Dvorscak
 *
 */
@Named
@Singleton
public class SystemConfigurationServiceImpl implements SystemConfigurationService {

    /**
     * @see #getServerHost()
     */
    private volatile String serverHost;

	/**
	 * @see #getServiceHttpPort()
	 */
	private volatile int serviceHttpPort;

	/**
	 * @see #getServiceHttpContextPath()
	 */
	private volatile String serviceHttpContextPath;

	/**
	 * @see #getVarDir()
	 */
	private volatile String varDir;

	/**
	 * @see #getJDBCDriver()
	 */
	private volatile String jdbcDriver;

	/**
	 * @see #getJDBCUrl()
	 */
	private volatile String jdbcUrl;

	/**
	 * @see #getJDBCUsername()
	 */
	private volatile String jdbcUsername;

	/**
	 * @see #getJDBCPassword()
	 */
	private volatile String jdbcPassword;

	/**
	 * @see #getHibernateDialect()
	 */
	private volatile String hibernateDialect;

	private volatile int urlConnectionTimeout;

	private volatile int urlConnectionReadTimeout;

    /**
	 * Constructor.
	 */
	public SystemConfigurationServiceImpl() {
	}

	/**
	 * Initializes service.
	 */
	@PostConstruct
	public void init() {
		Properties properties = loadProperties();
        serverHost = properties.getProperty(SERVER_HOST);
		varDir = properties.getProperty(VAR_DIR);
		jdbcDriver = properties.getProperty(JDBC_DRIVER);
		jdbcUrl = properties.getProperty(JDBC_URL);
		jdbcUsername = properties.getProperty(JDBC_USERNAME);
		jdbcPassword = properties.getProperty(JDBC_PASSWORD);
		hibernateDialect = properties.getProperty(HIBERNATE_DIALECT);
		serviceHttpPort = parseServiceHttpPort(properties.getProperty(SERVICE_HTTP_PORT));
		serviceHttpContextPath = properties.getProperty(SERVICE_HTTP_CONTEXT_PATH);
		urlConnectionTimeout = parseInteger(properties.getProperty(URL_CONNECTION_TIMEOUT, "5000"), URL_CONNECTION_TIMEOUT);
		urlConnectionReadTimeout = parseInteger(properties.getProperty(URL_CONNECTION_READ_TIMEOUT, "30000"), URL_CONNECTION_READ_TIMEOUT);
	}

	/**
	 * Parses {@link SystemConfigurationService#SERVICE_HTTP_PORT} configuration.
	 *
	 * @param serviceHttpPortValue
	 *            value loaded from the {@link SystemConfigurationService#PROPERTIES_FILE}.
	 * @return parsed value
	 */
	private int parseServiceHttpPort(String serviceHttpPortValue) {
		int result = parseInteger(serviceHttpPortValue, SERVICE_HTTP_PORT);

		if (result < 0 || result >= 65535) {
			throw new FatalApplicationException(String.format("Invalid configuration '%s' value: '%s'", SERVICE_HTTP_PORT,
					result));
		}

		return result;
	}

	private int parseInteger(String integerString, String field) {
		int result;
		if (integerString == null) {
			throw new FatalApplicationException(String.format("Missing configuration: '%s'", field));
		}
		try {
			result = Integer.parseInt(integerString);
		} catch (NumberFormatException e) {
			throw new FatalApplicationException(String.format("Invalid configuration '%s' value: '%s'", field,
					integerString));
		}

		return result;
	}


	/**
	 * Loads system properties file.
	 *
	 * @return system properties for the application
	 */
	private Properties loadProperties() {
        Properties result = new Properties();
        InputStream properties;

        try {
            properties = Files.exists(Paths.get(PROPERTIES_FILE))
                    ? Files.newInputStream(Paths.get(PROPERTIES_FILE))
                    : ClassLoader.getSystemResourceAsStream(PROPERTIES_FILE);
            
            if (properties == null) {
                throw new FatalApplicationException(String.format("Missing properties files: '%s'", PROPERTIES_FILE));
            }

            result.load(properties);
        } catch (IOException e) {
            throw new FatalApplicationException(String.format("Can not load properties file '%s': ", PROPERTIES_FILE), e);
        }
        return result;
	}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getServerHost() {
        return serverHost;
    }

    /**
	 * {@inheritDoc}
	 */
	@Override
	public String getVarDir() {
		return varDir;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getJDBCDriver() {
		return jdbcDriver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getJDBCUrl() {
		return jdbcUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getJDBCUsername() {
		return jdbcUsername;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getJDBCPassword() {
		return jdbcPassword;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHibernateDialect() {
		return hibernateDialect;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getServiceHttpPort() {
		return serviceHttpPort;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getServiceHttpContextPath() {
		return serviceHttpContextPath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getUrlConnectionTimeout() {
		return urlConnectionTimeout;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getUrlConnectionReadTimeout() {
		return urlConnectionReadTimeout;
	}
}
