package org.hotovo.lac.service.user;

import org.hotovo.lac.model.user.User;
import org.hotovo.lac.model.user.dto.UserDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface UserService {
    List<User> getAll();

    User getUserById(Long id);

    Optional<User> getUserByUserName(String username);

    void save(User user);

    void delete(User user);

    User login(String username, String password);

    void logout();

    boolean isUserAuthorized();

    boolean isAdminAuthorized();

    UserDTO toDTO(User user);

    UserDTO toDTO(Long id, String password, boolean initialPassword);

    User fromDTO(UserDTO user);

    String hashPassword(String password);
}
