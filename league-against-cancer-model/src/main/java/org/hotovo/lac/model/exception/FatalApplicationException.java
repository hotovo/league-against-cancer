package org.hotovo.lac.model.exception;

/**
 * Fatal exception - system will go down, as soon as possible, because it is unable to continue. E.g. unrecoverable transaction, missing
 * system configurations, full disk, etc...
 * 
 * @author Stanislav Dvorscak
 *
 */
public class FatalApplicationException extends ApplicationException {

	/**
	 * Serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 */
	public FatalApplicationException() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            {@link #getMessage()}
	 * @param cause
	 *            {@link #getCause()}
	 */
	public FatalApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            {@link #getMessage()}
	 */
	public FatalApplicationException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * 
	 * @param cause
	 *            {@link #getCause()}
	 */
	public FatalApplicationException(Throwable cause) {
		super(cause);
	}

}
