package org.hotovo.lac.assembly.standalone.javax.inject;

import org.hotovo.lac.service.javax.inject.Session;
import org.springframework.web.context.WebApplicationContext;

/**
 * JSR-330 scope resolver, extended by an application dependent scopes, which are not direct part of <i>javax.inject</i> standard.
 *
 * <ul>
 * <li>{@link Session}: {@link WebApplicationContext#SCOPE_SESSION}</li>
 * </ul>
 *
 * @author Stanislav Dvorscak
 *
 */
public class Jsr330ScopeMetadataResolver extends org.springframework.context.annotation.Jsr330ScopeMetadataResolver {

	/**
	 * Constructor.
	 */
	public Jsr330ScopeMetadataResolver() {
		registerScope(Session.class, WebApplicationContext.SCOPE_SESSION);
	}
}
