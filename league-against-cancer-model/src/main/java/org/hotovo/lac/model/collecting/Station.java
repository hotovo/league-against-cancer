package org.hotovo.lac.model.collecting;

import org.hotovo.lac.model.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Vladimir Hrusovsky
 */
@Entity
@Table(name = "stations")
public class Station extends BaseEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
