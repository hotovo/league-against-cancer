package org.hotovo.lac.assembly.standalone.module;

import org.hotovo.lac.service.common.filter.TooManyRequestsFilter;
import org.hotovo.lac.system.SystemConfigurationService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.client.HttpUrlConnectorProvider.ConnectionFactory;
import org.glassfish.jersey.client.filter.EncodingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.message.DeflateEncoder;
import org.glassfish.jersey.message.GZipEncoder;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.ext.ContextResolver;
import java.io.IOException;
import java.net.*;
import java.util.List;

/**
 * Loads REST Client module
 * 
 * @author Miroslav Stencel
 */

@Configuration
public class RestClientModule {

	@Inject
	private SystemConfigurationService systemConfigurationService;

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Client restClient() {
		ClientConfig config = new ClientConfig();
		config.connectorProvider(new HttpUrlConnectorProvider().connectionFactory(new ProxyConnectionFactory(systemConfigurationService)));

        return ClientBuilder.newClient(config)
				.register(JacksonFeature.class)
				.register(JacksonJaxbJsonProvider.class)
				.register(JaxbObjectMapperProvider.class)
				.register(EncodingFilter.class)
                .register(TooManyRequestsFilter.class)
				.register(GZipEncoder.class)
				.register(DeflateEncoder.class);
	}

	private static class ProxyConnectionFactory implements ConnectionFactory {

		private final SystemConfigurationService systemConfigurationService;

		public ProxyConnectionFactory(SystemConfigurationService systemConfigurationService) {
			this.systemConfigurationService = systemConfigurationService;
		}

		@Override
		public HttpURLConnection getConnection(URL url) throws IOException {
			try {
				Proxy proxy = null;

				List<Proxy> proxies = ProxySelector.getDefault().select(url.toURI());
				if (proxies != null) {
					proxy = proxies.get(0);
				}
				if (proxy == null) {
					proxy = Proxy.NO_PROXY;
				}
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection(proxy);
				urlConnection.setConnectTimeout(systemConfigurationService.getUrlConnectionTimeout());
				urlConnection.setReadTimeout(systemConfigurationService.getUrlConnectionReadTimeout());

				return urlConnection;
			} catch (URISyntaxException ex) {
				throw new IOException(ex);
			}
		}
	}

	public static class JaxbObjectMapperProvider implements ContextResolver<ObjectMapper> {

		private final ObjectMapper mapper;

		/**
		 * Constructor.
		 */
		public JaxbObjectMapperProvider() {
			this.mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
			mapper.registerModule(new JaxbAnnotationModule());
		}

		@Override
		public ObjectMapper getContext(Class<?> type) {
			return mapper;
		}

	}
}
