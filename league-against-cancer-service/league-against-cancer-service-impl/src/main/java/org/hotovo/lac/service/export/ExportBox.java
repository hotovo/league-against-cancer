package org.hotovo.lac.service.export;

/**
 * @author Vladimir Hrusovsky
 */
class ExportBox {
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
