package org.hotovo.lac.model.exception;

/**
 * Base class for all kinds of exceptions.
 * 
 * @author Stanislav Dvorscak
 *
 */
public class ApplicationException extends RuntimeException {

	/**
	 * Serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 */
	public ApplicationException() {
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            {@link #getMessage()}
	 * @param cause
	 *            {@link #getCause()}
	 */
	public ApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            {@link #getMessage()}
	 */
	public ApplicationException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * 
	 * @param cause
	 *            {@link #getCause()}
	 */
	public ApplicationException(Throwable cause) {
		super(cause);
	}

}
