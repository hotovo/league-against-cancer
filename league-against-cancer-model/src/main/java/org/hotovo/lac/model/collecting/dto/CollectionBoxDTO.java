package org.hotovo.lac.model.collecting.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
@XmlRootElement(name = "box")
public class CollectionBoxDTO {
    private Long id;
    private String number;
    private String recorderUserName;
    private String stationName;
    private List<RecordDTO> records;

    @XmlElement
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @XmlElement
    public String getRecorderUserName() {
        return recorderUserName;
    }

    public void setRecorderUserName(String recorderUserName) {
        this.recorderUserName = recorderUserName;
    }

    @XmlElement
    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @XmlElement
    public List<RecordDTO> getRecords() {
        return records;
    }

    public void setRecords(List<RecordDTO> records) {
        this.records = records;
    }
}
