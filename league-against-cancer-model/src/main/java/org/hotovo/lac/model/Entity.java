package org.hotovo.lac.model;

/**
 * @author Vladimir Hrusovsky
 */
public interface Entity {
	Long getId();
}
