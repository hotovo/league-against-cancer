package org.hotovo.lac.assembly.standalone;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;

/**
 * Appache commons daemon.
 * 
 * @author Stanislav Dvorscak
 *
 */
public class ApplicationDaemon implements Daemon {

	/**
	 * {@link Application} application.
	 */
	private volatile Application application = new Application();

	/**
	 * Constructor.
	 */
	public ApplicationDaemon() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(DaemonContext context) throws DaemonInitException, Exception {
		application = new Application();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		application.start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() {
		application.stop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		application = null;
	}

}
