package org.hotovo.lac.dao.collecting;

import org.hotovo.lac.model.collecting.CollectionBox;
import org.hotovo.lac.model.collecting.Station;
import org.hotovo.lac.model.user.User;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Transactional
@Singleton
public class StationDAOImpl implements StationDAO {
    @PersistenceContext
    private volatile EntityManager entityManager;

    @Override
    public List<Station> getAll() {
        return entityManager.createQuery("FROM Station", Station.class).getResultList();
    }

    @Override
    public Optional<Station> getStationByName(String name) {
        final List<Station> stations = entityManager.createQuery("SELECT station FROM Station AS station WHERE station.name=:name", Station.class)
                .setParameter("name", name)
                .getResultList();
        return stations.isEmpty() ? Optional.empty() : Optional.of(stations.get(0));
    }

    @Override
    public void save(Station station) {
        if (station.getId() == null) {
            entityManager.persist(station);
        } else {
            entityManager.merge(station);
        }
    }
}
