package org.hotovo.lac.service.exception;

/**
 * @author Vladimir Hrusovsky
 */
public class SynchronizationException extends RuntimeException {

	/**
	 * Serial version id.
	 */
	private static final long serialVersionUID = 1L;
	

	public SynchronizationException(String message) {
        super(message);
    }

    public SynchronizationException(String message, Throwable cause) {
        super(message, cause);
    }
}
