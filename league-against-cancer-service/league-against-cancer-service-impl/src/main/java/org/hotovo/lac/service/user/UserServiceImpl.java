package org.hotovo.lac.service.user;

import org.hotovo.lac.dao.user.UserDAO;
import org.hotovo.lac.model.exception.ApplicationException;
import org.hotovo.lac.model.user.User;
import org.hotovo.lac.model.user.dto.UserDTO;
import org.hotovo.lac.service.http.ApplicationSession;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class UserServiceImpl implements UserService {
    @Inject
    private volatile Provider<ApplicationSession> session;
    @Inject
    private volatile UserDAO userDAO;

    @Override
    public String hashPassword(String password) {
        if (password == null || password.isEmpty()) {
            return "";
        }

        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            final Charset charset = Charset.forName("UTF-8");
            return new String(messageDigest.digest(password.getBytes(charset)), charset);
        } catch (NoSuchAlgorithmException e) {
            throw new ApplicationException("Can not find 'SHA-256' algorithm:", e);
        }
    }

    @Override
    public List<User> getAll() {
        return userDAO.getAll();
    }

    @Override
    public User getUserById(Long id) {
        return userDAO.getUserById(id);
    }

    @Override
    public Optional<User> getUserByUserName(String username) {
        return userDAO.getUserByUserName(username);
    }

    @Override
    public void save(User user) {
        userDAO.save(user);
    }

    @Override
    public void delete(User user) {
        userDAO.delete(user);
    }

    @Override
    public User login(String username, String password) {
        final Optional<User> user = userDAO.getUserByUserName(username);
        final boolean authenticated = user.isPresent() && user.get().getPassword().equals(hashPassword(password));

        if (authenticated) {
            session.get().setUsername(username);
        }

        return authenticated ? user.get() : null;
    }

    @Override
    public void logout() {
        session.get().clear();
    }

    @Override
    public boolean isUserAuthorized() {
        return getSessionUsername() != null;
    }

    @Override
    public boolean isAdminAuthorized() {
		//check for admin authorization
        return isUserAuthorized();
    }

    private String getSessionUsername() {
        return session.get().getUsername();
    }

    @Override
    public UserDTO toDTO(User user) {
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUserName(user.getUserName());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }

    @Override
    public UserDTO toDTO(Long id, String password, boolean initialPassword) {
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(id);
        userDTO.setPassword(password);
        return userDTO;
    }

    @Override
    public User fromDTO(UserDTO userDTO) {
        final User user;
        if (userDTO.getId() != null) {
            user = userDAO.getUserById(userDTO.getId());
        } else {
            user = new User();
        }

        user.setUserName(userDTO.getUserName());
        user.setPassword(userDTO.getPassword());

        return user;
    }
}
