package org.hotovo.lac.service.collecting;

import org.hotovo.lac.model.collecting.Station;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface StationService {
    List<Station> getAll();

    Optional<Station> getStationByName(String name);

    void save(Station station);
}
