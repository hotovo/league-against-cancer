package org.hotovo.lac.dao.collecting;

import org.hotovo.lac.model.collecting.CollectionBox;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Transactional
public class CollectionBoxDAOImpl implements CollectionBoxDAO {
    @PersistenceContext
    private volatile EntityManager entityManager;

    @Override
    public List<CollectionBox> getAll() {
        return entityManager.createQuery("FROM CollectionBox", CollectionBox.class).getResultList();
    }

    @Override
    public void save(CollectionBox box) {
        if (box.getId() == null) {
            entityManager.persist(box);
        } else {
            entityManager.merge(box);
        }
    }

    @Override
    public Optional<CollectionBox> getBoxById(long id) {
        return Optional.ofNullable(entityManager.find(CollectionBox.class, id));
    }
}
