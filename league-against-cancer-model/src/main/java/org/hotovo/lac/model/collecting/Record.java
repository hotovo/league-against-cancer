package org.hotovo.lac.model.collecting;

import org.hotovo.lac.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Vladimir Hrusovsky
 */
@Entity
@Table(name = "records")
public class Record extends BaseEntity {
    private Float nominal;
    private Integer count;

    @Column(name = "nominal")
    public Float getNominal() {
        return nominal;
    }

    public void setNominal(Float nominal) {
        this.nominal = nominal;
    }

    @Column(name = "count_")
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
