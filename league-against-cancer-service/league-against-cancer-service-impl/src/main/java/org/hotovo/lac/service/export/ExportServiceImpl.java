package org.hotovo.lac.service.export;

import org.hotovo.lac.model.collecting.CollectionBox;
import org.hotovo.lac.service.collecting.CollectionBoxService;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class ExportServiceImpl implements ExportService {
    @Inject
    private volatile CollectionBoxService boxService;

    @Override
    public void exportVariantA(OutputStream outputStream) {
        exportAccounting(boxService.getAll(), outputStream, "accountingVariantA-template.xlsx");
    }

    @Override
    public void exportVariantB(OutputStream outputStream) {
        exportAccounting(boxService.getAll(), outputStream, "accountingVariantB-template.xlsx");
    }

    void exportAccounting(Collection<CollectionBox> boxes, OutputStream os, String templateName) {

        final List<Map<String, Object>> exportBoxes = boxes.parallelStream()
                .map(this::toMap)
                .collect(Collectors.toList());

        try (InputStream is = ExportServiceImpl.class.getResourceAsStream(templateName)) {
            Context context = new Context();
            context.putVar("boxes", exportBoxes);
            JxlsHelper.getInstance().processTemplate(is, os, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Map<String, Object> toMap(CollectionBox box) {
        final HashMap<String, Object> map = new HashMap<>();

        map.put("number", box.getNumber());
        box.getRecords().parallelStream()
                .filter(record -> record.getNominal() < 1)
                .forEach(record -> map.put("c" + String.valueOf((int) (record.getNominal() * 100)), record.getCount()));
        box.getRecords().parallelStream()
                .filter(record -> record.getNominal() >= 1)
                .forEach(record -> map.put("e" + String.valueOf(record.getNominal().intValue()), record.getCount()));

        return map;
    }
}
