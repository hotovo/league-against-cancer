package org.hotovo.lac.service.export;

import org.hotovo.lac.model.collecting.CollectionBox;
import org.hotovo.lac.model.collecting.Record;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author Vladimir Hrusovsky
 */
public class ExportServiceImplTest {
    @org.junit.Test
    public void exportAccounting() throws Exception {
        final ExportServiceImpl exportService = new ExportServiceImpl();
        exportService.exportAccounting(createTextBoxes(), new FileOutputStream("test.xls"), "accountingVariantA-template.xlsx");
    }

    private List<CollectionBox> createTextBoxes() {
        final List<CollectionBox> boxes = new ArrayList<>();

        for (int index = 0; index < 20; index++) {
            boxes.add(createTestBox());
        }

        return boxes;
    }

    private CollectionBox createTestBox() {
        final CollectionBox box = new CollectionBox();
        box.setNumber(UUID.randomUUID().toString().substring(0, 10));

        final List<Record> records = new ArrayList<>();
        box.setRecords(records);

        records.add(createTestRecord(0.01f));
        records.add(createTestRecord(0.02f));
        records.add(createTestRecord(0.05f));
        records.add(createTestRecord(0.1f));
        records.add(createTestRecord(0.2f));
        records.add(createTestRecord(0.5f));
        records.add(createTestRecord(1f));
        records.add(createTestRecord(2f));
        records.add(createTestRecord(5f));
        records.add(createTestRecord(10f));
        records.add(createTestRecord(20f));
        records.add(createTestRecord(50f));
        records.add(createTestRecord(100f));
        records.add(createTestRecord(200f));
        records.add(createTestRecord(500f));
        records.add(createTestRecord(1000f));

        return box;
    }

    private Record createTestRecord(float nominal) {
        final Record record = new Record();
        record.setNominal(nominal);
        record.setCount(new Random().nextInt(100));
        return record;
    }

}