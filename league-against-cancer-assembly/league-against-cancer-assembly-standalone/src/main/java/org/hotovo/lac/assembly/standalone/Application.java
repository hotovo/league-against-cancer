package org.hotovo.lac.assembly.standalone;

import org.hotovo.lac.assembly.standalone.module.*;
import org.hotovo.lac.assembly.standalone.service.HttpService;
import org.hotovo.lac.model.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Main launcher of the application.
 * 
 * @author Stanislav Dvorscak
 *
 */
public class Application {

	/**
	 * Logger for this class.
	 */
	private final Logger logger = LoggerFactory.getLogger(Application.class);

	/**
	 * Application context of this application.
	 */
	private final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();

	/**
	 * Shutdown hook of the application.
	 */
	private final Thread shutdownHook = new Thread(this::stop, "System Shutdown");

	/**
	 * Waits until application will start.
	 */
	private final CountDownLatch waitForStart = new CountDownLatch(1);

	/**
	 * Flag - if it is true application is going down.
	 */
	private final AtomicBoolean stopping = new AtomicBoolean();

	/**
	 * Starts the application.
	 */
	public void start() {
		Runtime.getRuntime().addShutdownHook(shutdownHook);

		logger.info("Starting application...");
		applicationContext.addApplicationListener(event -> {
			if (event instanceof ContextStartedEvent) {
				logger.info("Application STARTED.");
				waitForStart.countDown();
			}
		});
		applicationContext.register(SystemModule.class);
		applicationContext.register(JTAModule.class);
		applicationContext.register(JPAModule.class);
		applicationContext.register(JMSModule.class);
		applicationContext.register(DAOModule.class);
		applicationContext.register(ServiceModule.class);
		applicationContext.register(JAXRSModule.class);
		applicationContext.register(WebSocketModule.class);
		applicationContext.register(HttpService.class);
		applicationContext.register(RestClientModule.class);
		try {
			applicationContext.refresh();
			applicationContext.start();
		} catch (BeanCreationException e) {
			waitForStart.countDown();
			stop();
		}
		try {
			waitForStart.await();
		} catch (InterruptedException e) {
			if (!stopping.get()) {
				throw new ApplicationException(e);
			}
		}
	}

	/**
	 * Stops the application.
	 */
	public void stop() {
		// only one stop will be performed, all others will be ignored
		if (stopping.compareAndSet(false, true)) {
			if (!shutdownHook.equals(Thread.currentThread())) {
				Runtime.getRuntime().removeShutdownHook(shutdownHook);
			}
			logger.info("Stopping application...");
			applicationContext.close();
			logger.info("Application STOPPED.");
			logger.info("Good Bye and Have a Nice Day!");
		}
	}

	/**
	 * main()
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {
		Application application = new Application();
		application.start();
	}

}
