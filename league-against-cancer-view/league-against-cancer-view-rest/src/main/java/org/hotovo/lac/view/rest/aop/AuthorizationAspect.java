package org.hotovo.lac.view.rest.aop;

import org.hotovo.lac.service.user.UserService;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * @author Vladimir Hrusovsky
 */
@Aspect
@Named
@Singleton
public class AuthorizationAspect {
    @Inject
    private volatile UserService userService;

    @SuppressWarnings("EmptyMethod")
    @Pointcut("execution(public * org.hotovo.lac.view.rest.*Resource.*(..))")
    private void publicRestCall() {
    }

    @Before("publicRestCall() && @annotation(authorization)")
    public void authorizeRoleCall(Authorization authorization) {
        switch (authorization.value()) {
            case USER:
                authorizeUser();
                break;
            case ADMIN:
                authorizeAdmin();
                break;
            case PUBLIC:
                //no need to authorize call
                break;
            default:
                throw new WebApplicationException("Unknown role specified for authorization: " + authorization.value(),
                        Response.Status.UNAUTHORIZED.getStatusCode());
        }
    }

    @Before("publicRestCall() && !@annotation(Authorization)")
    public void authorizeUserRoleCall() {
//        authorizeUser();
    }

    private void authorizeUser() {
        if (!userService.isUserAuthorized()) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED.getStatusCode());
        }
    }

    private void authorizeAdmin() {
        if (!userService.isAdminAuthorized()) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED.getStatusCode());
        }
    }
}
