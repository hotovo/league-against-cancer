package org.hotovo.lac.model.user.dto;

import org.hotovo.lac.model.BaseEntityDTO;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Vladimir Hrusovsky
 */
@XmlRootElement(name = "user")
public class UserDTO extends BaseEntityDTO {
    private String userName;
    private String password;

    @XmlElement
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlElement
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
