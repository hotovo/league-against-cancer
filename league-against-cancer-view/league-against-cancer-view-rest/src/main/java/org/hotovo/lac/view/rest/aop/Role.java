package org.hotovo.lac.view.rest.aop;

/**
 * @author Vladimir Hrusovsky
 */
public enum Role {
    PUBLIC,
    ADMIN,
    USER
}
