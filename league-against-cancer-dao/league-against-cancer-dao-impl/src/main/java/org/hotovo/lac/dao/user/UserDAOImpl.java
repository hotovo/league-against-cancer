package org.hotovo.lac.dao.user;

import org.hotovo.lac.model.user.User;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Transactional
public class UserDAOImpl implements UserDAO {
    @PersistenceContext
    private volatile EntityManager entityManager;

    @Override
    @Transactional
    public List<User> getAll() {
        return entityManager.createQuery("FROM User", User.class).getResultList();
    }

    @Override
    public void save(User user) {
        if (user.getId() == null) {
            entityManager.persist(user);
        } else {
            entityManager.merge(user);
        }
    }

    @Override
    public void delete(User user) {
        entityManager.remove(user);
        user.setId(null);
    }

    @Override
    public User getUserById(long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public Optional<User> getUserByUserName(String userName) {
        final List<User> users = entityManager.createQuery("SELECT u FROM User AS u WHERE u.userName=:userName", User.class)
                .setParameter("userName", userName)
                .getResultList();
        return users.isEmpty() ? Optional.empty() : Optional.of(users.get(0));
    }
}
