package org.hotovo.lac.assembly.standalone.module;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jms.ConnectionFactory;
import javax.jms.Session;
import javax.transaction.TransactionManager;

import org.apache.activemq.ActiveMQXAConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.pool.XaPooledConnectionFactory;
import org.apache.activemq.usage.SystemUsage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jms.core.JmsTemplate;

import org.hotovo.lac.model.exception.FatalApplicationException;
import org.hotovo.lac.system.SystemConfigurationService;

/**
 * Setups JMS support.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Configuration
public class JMSModule {

	/**
	 * Localhost connection URL for the {@link #broker()}.
	 */
	private static final String BROKER_CONNECTION_URL = "vm://localhost?broker.persistent=false";

	/**
	 * Injected {@link SystemConfigurationService} dependency.
	 */
	@Inject
	private volatile SystemConfigurationService systemConfigurationService;

	/**
	 * Injected {@link TransactionManager} dependency.
	 */
	@Inject
	private volatile TransactionManager transactionManager;

	/**
	 * Constructor.
	 */
	public JMSModule() {
	}

	/**
	 * @return MQ broker.
	 */
	@Bean(name = "mqBroker", destroyMethod = "stop")
	public BrokerService broker() {
		BrokerService brokerService = new BrokerService();
		try {
			brokerService.addConnector(BROKER_CONNECTION_URL);
		} catch (Exception e) {
			throw new FatalApplicationException("Can not create MQ Broker connector:", e);
		}
		brokerService.setSystemUsage(systemUsage());
		brokerService.setDataDirectory(systemConfigurationService.getVarDir() + "/mq");
		return brokerService;
	}

	/**
	 * @return setups {@link SystemUsage} limits of the {@link #broker()}.
	 */
	private SystemUsage systemUsage() {
		SystemUsage result = new SystemUsage();
		result.getStoreUsage().setLimit(256 * 1024 * 1024);
		result.getTempUsage().setLimit(256 * 1024 * 1024);
		return result;
	}

	/**
	 * @return ActiveMQ JMS connection factory.
	 */
	public ActiveMQXAConnectionFactory connectionFactory() {
		ActiveMQXAConnectionFactory result = new ActiveMQXAConnectionFactory();
		result.setBrokerURL(BROKER_CONNECTION_URL);
		return result;
	}

	/**
	 * @return Pooled JMS connection factory used by system.
	 */
	@Bean(destroyMethod = "stop")
	@DependsOn("mqBroker")
	public ConnectionFactory pooledConnectionFactory() {
		XaPooledConnectionFactory result = new XaPooledConnectionFactory();
		result.setConnectionFactory(connectionFactory());
		result.setTransactionManager(transactionManager);
		return result;
	}

	/**
	 * Spring JMS template utility.
	 * 
	 * @param connectionFactory
	 *            JMS {@link ConnectionFactory} dependency.
	 * @return {@link JmsTemplate}
	 */
	@Bean
	@Singleton
	public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
		JmsTemplate result = new JmsTemplate();
		result.setReceiveTimeout(1000);
		result.setConnectionFactory(connectionFactory);
		result.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
		return result;
	}

}
