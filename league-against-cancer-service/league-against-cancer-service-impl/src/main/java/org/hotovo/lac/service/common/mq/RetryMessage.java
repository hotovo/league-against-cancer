package org.hotovo.lac.service.common.mq;

import org.hotovo.lac.model.common.mq.BaseMessage;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Vladimir Hrusovsky
 */
@XmlRootElement
public class RetryMessage extends BaseMessage {
    private String address;
    private String message;
    private long nextRetryTime;

    public RetryMessage() {
    }

    public RetryMessage(String address, String message, long nextRetryTime) {
        this.address = address;
        this.message = message;
        this.nextRetryTime = nextRetryTime;
    }

    @XmlElement
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @XmlElement
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @XmlElement
    public long getNextRetryTime() {
        return nextRetryTime;
    }

    public void setNextRetryTime(long nextRetryTime) {
        this.nextRetryTime = nextRetryTime;
    }
}
